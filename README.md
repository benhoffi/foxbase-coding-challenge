## Project Installation

```sh
git clone https://gitlab.com/benhoffi/foxbase-coding-challenge
```

## Move Into The Project Directory

```sh
cd foxbase-coding-challenge
```

## Project Setup

```sh
npm install
```

### To test the Codeing Challange

```sh
npm run dev
```
[http://localhost:3000/](http://localhost:3000/)
